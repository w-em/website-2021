import Vue from 'vue'
import { registerBackendLayouts } from '~typo3/plugins/layouts'
// news
const BeDefault = () =>
  import(/* webpackChunkName: "BeDefault" */ '~/layouts/backend/BeDefault')

const BeStartpage = () =>
  import(/* webpackChunkName: "BeStartpage" */ '~/layouts/backend/BeStartpage')

const layouts = {
  BeDefault,
  BeStartpage
}

export default ({ app }) => {
  Vue.use(registerBackendLayouts, {
    context: app,
    layouts
  })
}
