<?php

/***************************************************************
 * Extension Manager/Repository config file for ext "headless_gridelements".
 *
 * Auto generated 05-11-2020 14:43
 *
 * Manual updates:
 * Only the data in the array - everything else is removed by next
 * writing. "version" and "dependencies" must not be touched!
 ***************************************************************/

$EM_CONF[$_EXTKEY] = array(
    'title' => 'Headless Grid Elements',
    'description' => 'Grid Elements json output for EXT:headless',
    'category' => 'fe',
    'author' => 'Stefan Jaeger',
    'author_email' => 'sj@w-em.com',
    'author_company' => 'w-em GmbH',
    'state' => 'beta',
    'clearCacheOnLoad' => 0,
    'version' => '1.0.0',
    'constraints' =>
        array(
            'depends' =>
                array(
                    'typo3' => '10.0.0-10.4.99',
                    'headless' => '2.0.0-2.99.99',
                    'gridelements' => '10.0.0-10.99.99',
                ),
            'conflicts' =>
                array(),
            'suggests' =>
                array(),
        ),
    'autoload' =>
        array(
            'psr-4' =>
                array(
                    'Wem\\HeadlessGridelements\\' => 'Classes',
                ),
        ),
    'autoload-dev' =>
        array(
            'psr-4' =>
                array(
                    'Wem\\HeadlessGridelements\\Tests\\' => 'Tests',
                ),
        ),
    'uploadfolder' => false,
    'clearcacheonload' => false,
);

