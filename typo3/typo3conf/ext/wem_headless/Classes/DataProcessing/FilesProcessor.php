<?php

declare(strict_types=1);

namespace Wem\WemHeadless\DataProcessing;

use Wem\WemHeadless\Utility\FileUtility;
use TYPO3\CMS\Core\Resource\FileInterface;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Frontend\ContentObject\ContentObjectRenderer;
use TYPO3\CMS\Frontend\ContentObject\DataProcessorInterface;
use TYPO3\CMS\Frontend\Resource\FileCollector;
use FriendsOfTYPO3\Headless\Utility\FileUtility as BaseFileUtility;

/**
 * Class FilesProcessor
 */
class FilesProcessor extends \FriendsOfTYPO3\Headless\DataProcessing\FilesProcessor
{

    /**
     * Process data for a gallery, for instance the CType "textmedia"
     *
     * @param ContentObjectRenderer $cObj The content object renderer, which contains data of the content element
     * @param array $contentObjectConfiguration The configuration of Content Object
     * @param array $processorConfiguration The configuration of this processor
     * @param array $processedData Key/value store of processed data (e.g. to be passed to a Fluid View)
     * @return array the processed data as key/value store
     */
    public function process(
        ContentObjectRenderer $cObj,
        array $contentObjectConfiguration,
        array $processorConfiguration,
        array $processedData
    ) {
        $data = parent::process($cObj, $contentObjectConfiguration, $processorConfiguration, $processedData);

        if (isset($processorConfiguration['processingConfiguration.']) && isset($processorConfiguration['processingConfiguration.']['responsive.'])) {
            print_r($data);

            $targetFieldName = (string)$cObj->stdWrapValue(
                'as',
                $this->processorConfiguration,
                $this->defaults['as']
            );

            print_r($processorConfiguration['processingConfiguration.']['responsive.']);

            if (is_array($processorConfiguration['processingConfiguration.']['responsive.'])) {
                $images = [];
                $responsiveSettings = $processorConfiguration['processingConfiguration.']['responsive.'];
                foreach ($responsiveSettings as $key => $imageSettings) {

                    $dimensions = [
                        'width' => $processorConfiguration['processingConfiguration.']['responsive.'][$key]['width'] ?? null,
                        'height' => $processorConfiguration['processingConfiguration.']['responsive.'][$key]['height'] ?? null,
                        'minWidth' => $processorConfiguration['processingConfiguration.']['responsive.'][$key]['minWidth'] ?? null,
                        'minHeight' => $processorConfiguration['processingConfiguration.']['responsive.'][$key]['minHeight'] ?? null,
                        'maxWidth' => $processorConfiguration['processingConfiguration.']['responsive.'][$key]['maxWidth'] ?? null,
                        'maxHeight' => $processorConfiguration['processingConfiguration.']['responsive.'][$key]['maxHeight'] ?? null,
                    ];

                    $images[] = $this->processFiles($dimensions);
                }

                print_r($images);
            }
        }

        return $data;
    }

    protected function getFileUtility(): BaseFileUtility
    {
        return GeneralUtility::makeInstance(FileUtility::class);
    }
}
