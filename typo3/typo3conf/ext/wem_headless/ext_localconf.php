<?php
defined('TYPO3_MODE') || die('Access denied.');

// Gridelements
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig('<INCLUDE_TYPOSCRIPT: source="FILE:EXT:wem_headless/Configuration/TsConfig/GridElements.tsconfig">');


/***************
 * Register Icons
 */
$iconRegistry = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Imaging\IconRegistry::class);
$icons = [
    'slider',
    'slider-item',
];
foreach ($icons as $icon) {
    $iconRegistry->registerIcon(
        'content-wemheadless-' . $icon,
        \TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
        ['source' => 'EXT:wem_headless/Resources/Public/Icons/ContentElements/' . $icon . '.svg']
    );
}
