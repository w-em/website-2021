<?php
defined('TYPO3_MODE') || die('Access denied.');

$extensionKey = 'wem_headless_slider';

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile(
    'wem_headless_slider',
    'Configuration/TypoScript',
    'Slider json output for EXT:headless'
);
